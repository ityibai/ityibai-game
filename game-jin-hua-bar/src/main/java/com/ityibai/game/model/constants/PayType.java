package com.ityibai.game.model.constants;

/**
 * 下注类型 闷 直接下
 * Created by YinLin on 17/9/28.
 */
public enum PayType {
    /**
     * 闷抓
     */
    MUFFLED,
    /**
     * 已看牌平上
     */
    CHECKED
}
