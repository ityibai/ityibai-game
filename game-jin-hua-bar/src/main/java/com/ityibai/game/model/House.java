package com.ityibai.game.model;

import com.ityibai.game.model.pojo.PlayerVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 房间
 * Created by YinLin on 17/9/28.
 */
public class House extends BaseObject  {

    private Long id;

    /**
     * 该房间基础下注筹码
     */
    private long bet;

    /**
     * 该房间最高筹码
     */
    private long highBet;

    /**
     * 房主
     */
    private Player owner;



    /**
     * 当前游戏局
     */
    private Game currentGame;


    /**
     * 历史记录
     */
    private List<Game> history;

    /**
     * index
     */
    private BlockingQueue<Integer> indexes = new LinkedBlockingQueue<Integer>(17);

    /**
     * 位置
     */
    private List<PlayerVO> players = new ArrayList<PlayerVO>();

    public House(Player player) {
        this.owner = player;
        this.bet = 10L;
        this.highBet = 300L;
    }

    public long getBet() {
        return bet;
    }

    public void setBet(long bet) {
        this.bet = bet;
    }

    public long getHighBet() {
        return highBet;
    }

    public void setHighBet(long highBet) {
        this.highBet = highBet;
    }

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public List<PlayerVO> getPlayers() {
        return players;
    }

    public void addPlayer(PlayerVO player) {
        this.players.add(player);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPlayers(List<PlayerVO> players) {
        this.players = players;
    }

    public Game getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(Game currentGame) {
        this.currentGame = currentGame;
    }

    public List<Game> getHistory() {
        return history;
    }

    public void setHistory(List<Game> history) {
        this.history = history;
    }
}
