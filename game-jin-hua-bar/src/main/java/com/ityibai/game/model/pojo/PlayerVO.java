/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 *
 *			~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 	    	~		佛主保佑				    _oo0oo_		  永无bug			~
 * 			~						       o8888888o						~
 * 			~							   88" . "88						~
 * 			~							   (| -_- |)						~
 * 			~							   0\  =  /0						~
 * 			~							____/'---'\____						~
 * 			~						  .' \\|	   |// '.					~
 * 			~						 /	\\|||  :  |///   \					~
 *      	~                       /  _||||| -:- |||||   \					~
 *      	~                      |    | \\\  -  /// |    |				~
 *      	~                      |  \_|  ''\---/''  |_/  |				~
 *      	~                      \   .-\___ '-' ___/-.   /				~
 *      	~                    ___'.  .'  /--.--\  '.  .'___				~
 *      	~                 ."" '<  '.____\_<|>_/____.'  >'"".			~
 *      	~                | | :  '- \'.;  \ - /'; .'/  - ':| |			~
 *      	~                \ \  '-.   \_  __\ /__  _/   .-'/  /			~
 *      	~                 '-.____'.___  \_____/____.-'___.-'  			~
 *      	~                    											~
 *      	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       
 * 
 */
package com.ityibai.game.model.pojo;

import com.ityibai.game.model.BaseObject;
import com.ityibai.game.model.BoardResult;

import java.util.List;

/**
 * Created by panda on 2017/9/29.
 * Version : 1.0
 * Description : com.ityibai.game.model.pojo.PlayerVO
 */
public class PlayerVO extends BaseObject {

    private Long id;

    private String name;

    private BoardResult boardResult;

    private List<BoardResult> history;

    private int index;//位置

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BoardResult getBoardResult() {
        return boardResult;
    }

    public void setBoardResult(BoardResult boardResult) {
        this.boardResult = boardResult;
    }

    public List<BoardResult> getHistory() {
        return history;
    }

    public void setHistory(List<BoardResult> history) {
        this.history = history;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
