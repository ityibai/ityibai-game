package com.ityibai.game.model.constants;

/**
 * 牌面结果 混色 对子 顺子 同花 同花顺 豹子
 * Created by YinLin on 17/9/28.
 */
public enum ResultType {
    /**
     * 混色
     */
    MIXTURE(0),
    /**
     * 对子
     */
    PAIR(100),
    /**
     * 顺子
     */
    STRAIGHT(200),
    /**
     * 同花
     */
    FLUSH(300),
    /**
     * 同花顺
     */
    STRAIGHT_FLUSH(400),
    /**
     * 豹子
     */
    LEOPARD(500);

    int value;

    public int getValue() {
        return value;
    }

    ResultType(int value) {
        this.value = value;
    }
}
