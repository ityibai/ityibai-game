package com.ityibai.game.model.constants;

/**
 * 花色 黑桃 红桃 梅花 方块
 * Created by YinLin on 17/9/28.
 * @author panda
 */
public enum Color {
    /**
     * 黑桃
     */
    SPADE("♠"),
    /**
     * 红桃
     */
    HEART("♥"),
    /**
     * 梅花
     */
    CLUB("♣"),
    /**
     * 方块
     */
    DIAMOND("♦");

    String value;

    public String getValue() {
        return value;
    }

    Color(String value) {
        this.value = value;
    }
}
