package com.ityibai.game.model;

import com.ityibai.game.model.constants.Color;

/**
 * Created by YinLin on 17/9/28.
 */
public class Board extends BaseObject implements Comparable<Board> {

    private int point;

    private String name;

    private Color color;

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Board(int point, String name, Color color) {
        this.point = point;
        this.name = name;
        this.color = color;
    }

    public int compareTo(Board o) {
        return this.point > o.point ? 1 : this.point == o.point ? 0 : -1;
    }
}
