/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 *
 *			~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 	    	~		佛主保佑				    _oo0oo_		  永无bug			~
 * 			~						       o8888888o						~
 * 			~							   88" . "88						~
 * 			~							   (| -_- |)						~
 * 			~							   0\  =  /0						~
 * 			~							____/'---'\____						~
 * 			~						  .' \\|	   |// '.					~
 * 			~						 /	\\|||  :  |///   \					~
 *      	~                       /  _||||| -:- |||||   \					~
 *      	~                      |    | \\\  -  /// |    |				~
 *      	~                      |  \_|  ''\---/''  |_/  |				~
 *      	~                      \   .-\___ '-' ___/-.   /				~
 *      	~                    ___'.  .'  /--.--\  '.  .'___				~
 *      	~                 ."" '<  '.____\_<|>_/____.'  >'"".			~
 *      	~                | | :  '- \'.;  \ - /'; .'/  - ':| |			~
 *      	~                \ \  '-.   \_  __\ /__  _/   .-'/  /			~
 *      	~                 '-.____'.___  \_____/____.-'___.-'  			~
 *      	~                    											~
 *      	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       
 * 
 */
package com.ityibai.game.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;

/**
 * Created by panda on 2017/9/29.
 * Version : 1.0
 * Description : com.ityibai.game.model.BaseObject
 */
public abstract class BaseObject implements Serializable {
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }
}
