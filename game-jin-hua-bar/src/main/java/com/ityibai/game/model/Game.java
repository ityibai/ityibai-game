/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 *
 *			~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 	    	~		佛主保佑				    _oo0oo_		  永无bug			~
 * 			~						       o8888888o						~
 * 			~							   88" . "88						~
 * 			~							   (| -_- |)						~
 * 			~							   0\  =  /0						~
 * 			~							____/'---'\____						~
 * 			~						  .' \\|	   |// '.					~
 * 			~						 /	\\|||  :  |///   \					~
 *      	~                       /  _||||| -:- |||||   \					~
 *      	~                      |    | \\\  -  /// |    |				~
 *      	~                      |  \_|  ''\---/''  |_/  |				~
 *      	~                      \   .-\___ '-' ___/-.   /				~
 *      	~                    ___'.  .'  /--.--\  '.  .'___				~
 *      	~                 ."" '<  '.____\_<|>_/____.'  >'"".			~
 *      	~                | | :  '- \'.;  \ - /'; .'/  - ':| |			~
 *      	~                \ \  '-.   \_  __\ /__  _/   .-'/  /			~
 *      	~                 '-.____'.___  \_____/____.-'___.-'  			~
 *      	~                    											~
 *      	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       
 * 
 */
package com.ityibai.game.model;

import com.ityibai.game.model.pojo.PlayerVO;

/**
 * Created by panda on 2017/9/29.
 * Version : 1.0
 * Description : com.ityibai.game.model.Game
 */
public class Game extends BaseObject {

    private Long id;
    /**
     * 房间号
     */
    private House house;

    /**
     * 胜利者
     */
    private PlayerVO winner;

    /**
     * 是否结束
     */
    private boolean end;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public PlayerVO getWinner() {
        return winner;
    }

    public void setWinner(PlayerVO winner) {
        this.winner = winner;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }
}
