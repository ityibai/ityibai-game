package com.ityibai.game.model;

/**
 * Created by YinLin on 17/9/28.
 */
public class Player extends BaseObject  {

    private Long id;

    private String name;

    /**
     * 金币
     */
    private Long coin;

    public Long getCoin() {
        return coin;
    }

    public void setCoin(Long coin) {
        this.coin = coin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
