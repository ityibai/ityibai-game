package com.ityibai.game.model;

import com.ityibai.game.model.constants.ResultType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by YinLin on 17/9/28.
 */
public class BoardResult extends BaseObject  {
    /**
     * 三张牌
     */
    private List<Board> boards = new ArrayList<Board>();

    /**
     * 获得牌的最终结果
     */
    private ResultType type;

    /**
     * 排面比分
     */
    private int score;

    public List<Board> getBoards() {
        return boards;
    }

    public void setBoards(List<Board> boards) {
        this.boards = boards;
    }

    public ResultType getType() {
        return type;
    }

    public void setType(ResultType type) {
        this.type = type;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
