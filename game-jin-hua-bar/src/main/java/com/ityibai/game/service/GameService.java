package com.ityibai.game.service;

import com.ityibai.game.model.BoardResult;
import com.ityibai.game.model.Player;

import java.util.List;

/**
 * Created by YinLin on 17/9/28.
 */
public interface GameService {

    /**
     * 进房间
     * @param player
     * @param houseId
     * @return
     */
    int enterHouse(Player player, int houseId);

    /**
     * 设置注码
     * @param cash
     */
    void setBet(Player player, int houseId, long cash);

    /**
     * 准备
     * @param player
     * @param houseId
     */
    void prepare(Player player, int houseId);

    /**
     * 发牌
      * @param players
     */
    void play(List<Player> players);

    /**
     * 下注
     * @param player
     * @param cash
     */
    void pay(Player player, long cash);

    /**
     * 看牌
     * @param player
     * @return
     */
    BoardResult check(Player player);

    boolean open(Player opener, Player compare);
}
