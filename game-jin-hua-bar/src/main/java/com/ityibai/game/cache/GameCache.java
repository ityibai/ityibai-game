/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 *
 *			~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 	    	~		佛主保佑				    _oo0oo_		  永无bug			~
 * 			~						       o8888888o						~
 * 			~							   88" . "88						~
 * 			~							   (| -_- |)						~
 * 			~							   0\  =  /0						~
 * 			~							____/'---'\____						~
 * 			~						  .' \\|	   |// '.					~
 * 			~						 /	\\|||  :  |///   \					~
 *      	~                       /  _||||| -:- |||||   \					~
 *      	~                      |    | \\\  -  /// |    |				~
 *      	~                      |  \_|  ''\---/''  |_/  |				~
 *      	~                      \   .-\___ '-' ___/-.   /				~
 *      	~                    ___'.  .'  /--.--\  '.  .'___				~
 *      	~                 ."" '<  '.____\_<|>_/____.'  >'"".			~
 *      	~                | | :  '- \'.;  \ - /'; .'/  - ':| |			~
 *      	~                \ \  '-.   \_  __\ /__  _/   .-'/  /			~
 *      	~                 '-.____'.___  \_____/____.-'___.-'  			~
 *      	~                    											~
 *      	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       
 * 
 */
package com.ityibai.game.cache;

import com.ityibai.game.model.House;
import com.ityibai.game.model.Player;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by panda on 2017/9/29.
 * Version : 1.0
 * Description : com.ityibai.game.cache.GameCache
 */
public class GameCache {
    /**
     * 游戏玩家Cache
     */
    public static ConcurrentHashMap<Long, Player> PLAYER_CACHE = new ConcurrentHashMap<Long, Player>();

    /**
     * 游戏房间Cache
     */
    public static ConcurrentHashMap<Long, House> HOUSE_CACHE = new ConcurrentHashMap<Long, House>();
}
