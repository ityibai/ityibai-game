/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 */
package com.ityibai.game.processor;

/**
 * Created by panda on 2017/10/13.
 * Version : 1.0
 * Description : com.ityibai.game.processor.GameProcessor
 */
public interface GameProcessor {

    void send(int houseId);

}
