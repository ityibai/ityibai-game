/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2011-2017 BaiQianJinRong and/or its affiliates. All rights reserved.
 *
 *
 *			~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 	    	~		佛主保佑				    _oo0oo_		  永无bug			~
 * 			~						       o8888888o						~
 * 			~							   88" . "88						~
 * 			~							   (| -_- |)						~
 * 			~							   0\  =  /0						~
 * 			~							____/'---'\____						~
 * 			~						  .' \\|	   |// '.					~
 * 			~						 /	\\|||  :  |///   \					~
 *      	~                       /  _||||| -:- |||||   \					~
 *      	~                      |    | \\\  -  /// |    |				~
 *      	~                      |  \_|  ''\---/''  |_/  |				~
 *      	~                      \   .-\___ '-' ___/-.   /				~
 *      	~                    ___'.  .'  /--.--\  '.  .'___				~
 *      	~                 ."" '<  '.____\_<|>_/____.'  >'"".			~
 *      	~                | | :  '- \'.;  \ - /'; .'/  - ':| |			~
 *      	~                \ \  '-.   \_  __\ /__  _/   .-'/  /			~
 *      	~                 '-.____'.___  \_____/____.-'___.-'  			~
 *      	~                    											~
 *      	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       
 * 
 */
package com.ityibai.game.processor;

import com.ityibai.game.model.Board;
import com.ityibai.game.model.BoardResult;
import com.ityibai.game.model.constants.CardConstants;
import com.ityibai.game.model.constants.Color;
import com.ityibai.game.model.constants.ResultType;
import com.ityibai.game.model.pojo.PlayerVO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by panda on 2017/10/13.
 * Version : 1.0
 * Description : com.ityibai.game.processor.GrameProcessorImpl
 * @author panda
 */
public class GrameProcessorImpl implements GameProcessor {
    /**
     * 洗牌返回
     * @return
     */
    private List<Board> shuffle() {
        List<Board> boards = new ArrayList<Board>();
        for (Color color : Color.values()) {
            for (int i = 0; i < CardConstants.CARDS.length; i++) {
                boards.add(new Board(CardConstants.POINTS[i],CardConstants.CARDS[i], color));
            }
        }
        //洗牌
        Collections.shuffle(boards);
        return boards;
    }

    /**
     * 发牌
     * @param houseId
     */
    @Override
    public void send(int houseId) {
        //洗牌
        List<Board> boards = shuffle();
        //获取人数
        List<PlayerVO> playerVOS = getPreparePlayers(houseId);
        if (playerVOS.size() < CardConstants.MIN_PLAYER_COUNT) {
            throw new RuntimeException("人数不足两人不能开始游戏");
        }
        if (playerVOS.size() > CardConstants.MAX_PLAYER_COUNT) {
            throw new RuntimeException("总人数大于17人不能开始游戏");
        }
        //正式发牌
        send(playerVOS, boards);
    }

    /**
     * 发牌操作
     * @param playerVOS
     * @param boards
     */
    private void send(List<PlayerVO> playerVOS, List<Board> boards) {
        int index = 0;
        //三轮发牌
        for (int i = 0; i < CardConstants.SEND_LOOP_NUM; i++) {
            for (PlayerVO playerVO : playerVOS) {
                if (i == 0) {
                    BoardResult boardResult = new BoardResult();
                    playerVO.setBoardResult(boardResult);
                }
                //顺序发牌给玩家
                playerVO.getBoardResult().getBoards().add(boards.get(index));
                if (i == 2) {
                    resultCount(playerVO);
                }
                index ++;
            }
        }
    }

    /**
     * 得分计算
     * @param playerVO
     */
    private void resultCount(PlayerVO playerVO) {
        ResultType resultType = checkType(playerVO.getBoardResult());
        int boardCount = 0;
        for (Board board : playerVO.getBoardResult().getBoards()) {
            //判断是否有带A 如果带A A=32
            boardCount += (board.getPoint() == 13 ? 32 : board.getPoint());
        }
        playerVO.getBoardResult().setType(resultType);
        playerVO.getBoardResult().setScore(resultType.getValue() + boardCount);
    }

    /**
     * 判断返回结果类型
     * @param boardResult
     * @return
     */
    private ResultType checkType(BoardResult boardResult) {
        Collections.sort(boardResult.getBoards());
        //第一张牌
        Board board1 = boardResult.getBoards().get(0);
        //第二张牌
        Board board2 = boardResult.getBoards().get(1);
        //第三张牌
        Board board3 = boardResult.getBoards().get(2);
        //1.豹子判断对子和豹子
        if (board1.getPoint() == board2.getPoint() && board1.getPoint() == board3.getPoint()) {
            return ResultType.LEOPARD;
        }
        //2.判断顺金和顺子 注意判断A的两种状态
        boolean isFirstStraightStep = ((board1.getPoint() - board2.getPoint()) == CardConstants.MIN_STRAIGHT_CHECK_STEP ||
                ((board1.getPoint() - board2.getPoint()) == CardConstants.MAX_STRAIGHT_CHECK_STEP));
        boolean isSecondStraightStep = ((board2.getPoint() - board3.getPoint()) == 1);
        if (isFirstStraightStep && isSecondStraightStep) {
            //判断是否花色相同
            if (board1.getColor().equals(board2.getColor()) && board1.getColor().equals(board3.getColor())) {
                return ResultType.STRAIGHT_FLUSH;
            } else {
                return ResultType.STRAIGHT;
            }
        }

        //3.判断同花
        if (board1.getColor().equals(board2.getColor()) && board1.getColor().equals(board3.getColor())) {
            return ResultType.FLUSH;
        }

        //4.判断对子
        if (board1.getPoint() == board2.getPoint() || board1.getPoint() == board3.getPoint() || board2.getPoint() == board3.getPoint()) {
            return ResultType.PAIR;
        }
        //返回混色
        return ResultType.MIXTURE;
    }

    private List<PlayerVO> getPreparePlayers(int houseId) {
        return null;
    }
}
